<?php include('includes/header.php'); ?>
	
<div id="pageWrap">
	<header>
		<div class="pageTitle">
			<h1>Summer vibes</h1>
			<h2>Share your favorite destination or find inspiration for your journey</h2>
		</div>
	</header>
	
	<section class="main">
		<div class="featured">
			<div class="featured-item" style="background:url(https://source.unsplash.com/HSj7Verc0cE/700x400);">	
				<h2>Spain</h2>
				<h3>Beautiful sunny country</h3>
				<a href="#_">Show Trips</a>
			</div>
			
			<div class="featured-item" style="background:url(https://source.unsplash.com/aapSemzfsOk/700x400);">
				<h2>Greece</h2>
				<h3>History's hidden gem</h3>
				<a href="#_">Show Trips</a>
			</div>
			
			<div class="featured-item" style="background:url(https://source.unsplash.com/d71ndCJzpUE/700x400);">
				<h2>Italy</h2>
				<h3>Referred to as "The Boot"</h3>
				<a href="#_">Show Trips</a>
			</div>
			
			<div class="featured-item" style="background:url(https://source.unsplash.com/5S6AizGYyOo/700x400);">
				<h2>Crotaia</h2>
				<h3>Contains thousand islands</h3>
				<a href="#_">Show Trips</a>
			</div>
		</div>
	</section>
	
</div>

<?php include('includes/footer.php'); ?>